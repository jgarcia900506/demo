package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.pojo.Pedido;
import com.example.demo.dao.pojo.PedidoDetalle;
import com.example.demo.dao.repository.PedidoDetalleRespository;
import com.example.demo.dao.repository.PedidoRepository;
import com.example.demo.service.PedidosService;
import com.example.demo.service.helper.ConvertHelper;
import com.example.demo.service.helper.PedidoHelper;
import com.example.demo.service.model.PedidoModel;

@Service
public class PedidosServiceImpl implements PedidosService {

	@Autowired
	private PedidoRepository					repositoryA;

	@Autowired
	private PedidoDetalleRespository			repositoryB;

	private ConvertHelper<Pedido, PedidoModel>	convert = PedidoHelper.getInstance();

	@Override
	public Iterable<PedidoModel> listAll() {
		return convert.lstPojoToLstModel(repositoryA.findAll());
	}

	@Override
	public PedidoModel save(PedidoModel model) {
		Pedido			pojo	= convert.modelToPojo(model);
		PedidoDetalle	child	= pojo.getDetalle();

		pojo.setDetalle(null);
		pojo = repositoryA.save(pojo);

		child.setPedido(pojo);
		repositoryB.save(child);

		return convert.pojoToModel(pojo);
	}

}
