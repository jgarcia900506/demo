package com.example.demo.service.model;

import java.io.Serializable;
import java.util.Date;

public class PedidoModel implements Serializable {

	/***/
	private static final long serialVersionUID = 1463653937402161389L;

	private Long	id;
	private Double	total;
	private Date	dateSale;
	private String	username;
	private PedidoDetalleModel detalle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getDateSale() {
		return dateSale;
	}

	public void setDateSale(Date dateSale) {
		this.dateSale = dateSale;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public PedidoDetalleModel getDetalle() {
		return detalle;
	}

	public void setDetalle(PedidoDetalleModel detalle) {
		this.detalle = detalle;
	}

}
