package com.example.demo.service.model;

import java.io.Serializable;

public class PedidoDetalleModel implements Serializable {

	private Long id;
	private String sku;
	private Double amout;
	private Double price;

	/***/
	private static final long serialVersionUID = -3879529797439518541L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Double getAmout() {
		return amout;
	}

	public void setAmout(Double amout) {
		this.amout = amout;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}
