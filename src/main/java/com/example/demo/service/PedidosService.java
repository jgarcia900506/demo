package com.example.demo.service;

import com.example.demo.service.model.PedidoModel;

public interface PedidosService {

	Iterable<PedidoModel> listAll();

	PedidoModel save(PedidoModel model);
}
