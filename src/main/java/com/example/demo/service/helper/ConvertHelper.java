package com.example.demo.service.helper;

public interface ConvertHelper<P, M> {

	M pojoToModel(P p);

	Iterable<M> lstPojoToLstModel(Iterable<P> lp);

	P modelToPojo(M m);

}
