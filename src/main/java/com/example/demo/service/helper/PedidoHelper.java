package com.example.demo.service.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.BeanUtils;

import com.example.demo.dao.pojo.Pedido;
import com.example.demo.dao.pojo.PedidoDetalle;
import com.example.demo.service.model.PedidoDetalleModel;
import com.example.demo.service.model.PedidoModel;

public class PedidoHelper implements ConvertHelper<Pedido, PedidoModel>{

	private static PedidoHelper instance = null;
	
	private PedidoHelper() { }
	
	@Override
	public PedidoModel pojoToModel(Pedido pojo) {
		PedidoModel model = new PedidoModel();
		BeanUtils.copyProperties(pojo, model);

		if(Objects.nonNull(pojo.getDetalle())) {
			PedidoDetalleModel child = new PedidoDetalleModel();
			BeanUtils.copyProperties(pojo.getDetalle(), child);
			
			model.setDetalle(child);
		}

		return model;
	}

	@Override
	public Iterable<PedidoModel> lstPojoToLstModel(Iterable<Pedido> lp) {
		List<PedidoModel> models = new ArrayList<>();
		lp.forEach(p -> models.add(pojoToModel(p)));
		
		return models;
	}

	public static PedidoHelper getInstance() {
		if(Objects.isNull(instance)) {
			instance = new PedidoHelper();
		}
		
		return instance;
	}

	@Override
	public Pedido modelToPojo(PedidoModel model) {
		Pedido pojo = new Pedido();
		BeanUtils.copyProperties(model, pojo);

		if(Objects.nonNull(model.getDetalle())) {
			PedidoDetalle child = new PedidoDetalle();
			BeanUtils.copyProperties(model.getDetalle(), child);
			
			pojo.setDetalle(child);
		}

		return pojo;
	}

}
