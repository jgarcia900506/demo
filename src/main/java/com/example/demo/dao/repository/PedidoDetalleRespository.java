package com.example.demo.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.pojo.PedidoDetalle;

@Repository
public interface PedidoDetalleRespository extends CrudRepository<PedidoDetalle, Long>{

}
