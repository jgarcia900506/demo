package com.example.demo.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.pojo.Pedido;

@Repository
public interface PedidoRepository extends CrudRepository<Pedido, Long> {

}
