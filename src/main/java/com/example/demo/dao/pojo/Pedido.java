package com.example.demo.dao.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pedidos_w")
public class Pedido {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long	id;

	@Column
	private Double	total;

	@Column
	private Date	dateSale;

	@Column
	private String	username;

	@OneToOne(mappedBy = "pedido")
	private PedidoDetalle detalle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getDateSale() {
		return dateSale;
	}

	public void setDateSale(Date dateSale) {
		this.dateSale = dateSale;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public PedidoDetalle getDetalle() {
		return detalle;
	}

	public void setDetalle(PedidoDetalle detalle) {
		this.detalle = detalle;
	}

}
